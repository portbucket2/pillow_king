﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class EnemyController : MonoBehaviour
{
    public static EnemyController Instance;
    public bool enemyHit;
    public int maxHealth = 100;
    public int currentHealth;
    public SliderScriptOne healthBar;

    [SerializeField]
    private TextMeshProUGUI m_healthText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    void Start()
    {
        enemyHit = false;
        //m_healthText = FindObjectOfType<TextMeshProUGUI>();
        //currentHealth = maxHealth;
        //healthBar.SetMaxHealth(maxHealth);
    }


    void Update()
    {
    }
        
}
