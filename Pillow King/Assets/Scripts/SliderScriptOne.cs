﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderScriptOne : MonoBehaviour
{
    public Slider slider;
    public TextMeshProUGUI healthValue;
    public void SetHealth(int health)
    {
        slider.value = health;
        healthValue.text = health.ToString();
    }
    public void SetMaxHealth(Unit unit)
    {
        healthValue.text = unit.maxHP.ToString();
        slider.maxValue = unit.maxHP;
        slider.value = unit.currentHP;
    }
}
