﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance;
    [SerializeField]
    public List<GameObject> m_Enemies = new List<GameObject>();
    
    [SerializeField]
    private Transform enemySpawnPoint;
    private int m_EnemyNo = 0;
    public bool enemyListEnded;

    private void Start()
    {
        enemyListEnded = false;
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void LoadLevel()
    {
        if(m_Enemies.Any())
        {
            Instantiate(m_Enemies[m_EnemyNo], enemySpawnPoint);
            Debug.Log("Enemy " +m_EnemyNo + "Loaded");
            m_Enemies.RemoveAt(m_EnemyNo);
        }
        else
        {
            enemyListEnded = true;

        }
    }
}
