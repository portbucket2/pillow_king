﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTransition : MonoBehaviour
{
    #region
    public static CameraTransition Instance;
    public Animator anim;
    #endregion
    #region Private Variables
    [SerializeField]
    private Transform[] targets;
    [SerializeField]
    private float m_transitionSpeed;
    private Transform currentView;


    #endregion
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

    }
    void Start()
    {
        currentView = targets[0];
        //enemyCam = false;
    }

    

    public void EnemyTurn()
    {
        //CameraTransitionToTarget(13f, 10f, -10f, 10f, -50f, 0f);
        
    }
    public void PlayerTurn()
    {
        //CameraTransitionToTarget(-13f, 10f, -10f, 10f, 50f, 0f);
        //anim.SetTrigger("PlayerTurn");
    }

    void CameraTransitionToTarget(float posX, float posY, float posZ, float rotX, float rotY, float rotZ)
    {
        transform.position = Vector3.Lerp(transform.position, new Vector3(posX, posY, posZ), Time.deltaTime * m_transitionSpeed);
        Vector3 currentangle = new Vector3(
            Mathf.LerpAngle(transform.rotation.eulerAngles.x, rotX, Time.deltaTime * m_transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.y, rotY, Time.deltaTime * m_transitionSpeed),
            Mathf.LerpAngle(transform.rotation.eulerAngles.z, rotZ, Time.deltaTime * m_transitionSpeed));

        transform.eulerAngles = currentangle;
    }
    
    
}
