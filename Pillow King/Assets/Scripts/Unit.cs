﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Unit : MonoBehaviour
{
    //public string unitName;
    //public int unitLevel;
    public static Unit Instance;
    public bool damage;

    public int maxHP;
    public int currentHP;
    public bool died;
    public Animator anim;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
    }
    private void Start()
    {
        died = false;
        damage = false;
    }

    public void TakeDamage(float value)
    {
        
        currentHP = Mathf.RoundToInt(currentHP - 40 * value);
        //Debug.Log(currentHP);
        damage = true;
        if (currentHP <= 0)
        {
            currentHP = 0;
            died = true;
        }
        else
        {
            died = false;
        }
    }
}
