﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateMachine : MonoBehaviour
{
    #region Private Variables
    public IState m_currentState;
    private IState m_previousState;
    #endregion 
    public void ChangeState(IState newState)
    {
        if (this.m_currentState != null)
            this.m_currentState.Exit();

        this.m_previousState = this.m_currentState;
        this.m_currentState = newState;
        this.m_currentState.Enter();
    }

    public void ExecuteStateFunction()
    {
        var runningState = this.m_currentState;
        if (runningState != null)
            runningState.Execute();

    }
    public void SwitchToPreviousState()
    {
        this.m_currentState.Exit();
        this.m_currentState = this.m_previousState;
        this.m_currentState.Enter();
    }
}
