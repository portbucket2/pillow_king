﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour
{
    public static PlayerController Instance;
    public PowerBarController powerBar;
    public int maxHealth = 100;
    public int currentHealth;
    public SliderScriptOne healthBar;
    public bool playerHit;

    #region Private Variables
    [SerializeField]
    private TextMeshProUGUI m_healthText;
    
    #endregion

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }


    IEnumerator Start()
    {
        playerHit = false;
        m_healthText = FindObjectOfType<TextMeshProUGUI>();
        //currentHealth = maxHealth;
        //healthBar.SetMaxHealth(maxHealth);
        GlobalTouchController.Instance.EnableTouchController();
        yield return new WaitForSeconds(1f);
        //powerBar.OnPassingTheAccuracyEvaluation += HitResult;
        powerBar.ShowPowerBar();
    }
    private void HitResult(float value)
    {
        playerHit = true;
        currentHealth = Mathf.RoundToInt(currentHealth - 40 * value);
        healthBar.SetHealth(currentHealth);
        m_healthText.SetText(currentHealth.ToString());
    }
}
