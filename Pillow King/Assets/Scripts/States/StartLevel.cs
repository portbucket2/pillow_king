﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;

public class StartLevel : IState
{

    #region Private Variables
    //private GameObject m_playerPrefab;
    //
    //private GameObject m_enemyPrefab;
    private TextMeshProUGUI m_startText;
    private GameObject tapToStartBtn;
    private GameObject m_levelComplete;
    private PowerBarController powerBar;


    #endregion

    public StartLevel( TextMeshProUGUI text, GameObject button, GameObject lvlComplete, PowerBarController pB)
    {
        
        this.m_startText = text;
        this.tapToStartBtn = button;
        this.m_levelComplete = lvlComplete;
        this.powerBar = pB;
    }
    public void Enter()
    {
        tapToStartBtn.SetActive(true);
        m_startText.enabled = true;
        LevelManager.Instance.LoadLevel();
        if (m_levelComplete != null)
            m_levelComplete.SetActive(false);
        TapToStartAnimate.Instance.DoAnimation();
    }

    public void Execute()
    {
        powerBar.ShowPowerBar();
    }

    public void Exit()
    {
        m_startText.enabled = false;
        tapToStartBtn.SetActive(false);
    }

    
}
