﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;
using TMPro;

public class Playerturn : IState
{
    [SerializeField]
    private int maxHealth;
    [SerializeField]
    private int currentHealth;
    [SerializeField]
    private SliderScriptOne healthBar;
    [SerializeField]
    private TextMeshProUGUI m_healthText;
    [SerializeField]
    private PowerBarController powerBar;
    private TextMeshProUGUI m_startText;
    private GameObject m_levelComplete;

    public Playerturn(int maxHP, int CurrentHP, SliderScriptOne bar, TextMeshProUGUI healthTxt, PowerBarController pB, TextMeshProUGUI startTxt, GameObject lvlComp)
    {
        this.maxHealth = maxHP;
        this.currentHealth = CurrentHP;
        this.healthBar = bar;
        this.m_healthText = healthTxt;
        this.powerBar = pB;
        this.m_startText = startTxt;
        this.m_levelComplete = lvlComp;
    }
    public void Enter()
    {
       
        //currentHealth = maxHealth;
        //healthBar.SetMaxHealth(maxHealth);
        GlobalTouchController.Instance.EnableTouchController();
        
        //powerBar.OnPassingTheAccuracyEvaluation += HitResult;
        powerBar.ShowPowerBar();
    }

    public void Execute()
    {
        
    }

    public void Exit()
    {
        powerBar.HidePowerBar();
    }

    private void HitResult(float value)
    {
        //playerHit = true;
        currentHealth = Mathf.RoundToInt(currentHealth - 40 * value);
        healthBar.SetHealth(currentHealth);
        m_healthText.SetText(currentHealth.ToString());
    }

}
