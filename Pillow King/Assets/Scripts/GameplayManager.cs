﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;
public class GameplayManager : MonoBehaviour
{

    #region Public Variables
    public static GameplayManager Instance;
    #endregion

    #region Private Variables
    private GameStateMachine stateMachine = new GameStateMachine();
    [SerializeField]
    private TextMeshProUGUI startText;
    [SerializeField]
    private GameObject tapButton;
    [SerializeField]
    private Button m_tapToStartButton;
    [SerializeField]
    private GameObject levelCompletePanel;
    [SerializeField]
    private PowerBarController powerBar;
    public int maxHealth = 100;
    public int currentHealth;
    public SliderScriptOne healthBar;
    [SerializeField]
    private TextMeshProUGUI m_healthText;



    #endregion
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;

        
        


    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log(this.stateMachine.m_currentState);
        this.stateMachine.ExecuteStateFunction();
    }

    
}
