﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using com.alphapotato.Gameplay;
using com.faithstudio.Gameplay;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WON, LOSE }
public class BattleSystem : MonoBehaviour
{
    public static BattleSystem Instance;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;

    public Transform playerSpawnpoint;
    public Transform enemySpawnpoint;


    Unit playerUnit;
    Unit enemyUnit;

    public SliderScriptOne playerHUD;
    public SliderScriptOne enemyHUD;

    public bool playerAttacked;
    public bool enemyAttacked;

    public TextMeshProUGUI startText;

    public PowerBarController powerBar;

    public BattleState state;
    public GameObject YouWon;
    public GameObject theButton;

    public LevelManager lvlmanager;

    public GameObject faceIconOne;
    public GameObject faceiconTwo;

    public TextMeshProUGUI lvlText;
    

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        
    }
    private void Start()
    {
        state = BattleState.START;
        StartCoroutine(SetupBattle());

        DontDestroyOnLoad(this.gameObject);
        playerAttacked = false;
        enemyAttacked = false;

    }

    IEnumerator SetupBattle()
    {
        GameObject playerGO = Instantiate(playerPrefab, playerSpawnpoint);
        playerUnit = playerGO.GetComponent<Unit>();
        //GameObject enemyGO = Instantiate(enemyPrefab, enemySpawnpoint);
        lvlmanager.LoadLevel();
        enemyUnit = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Unit>();

        playerHUD.SetMaxHealth(playerUnit);
        enemyHUD.SetMaxHealth(enemyUnit);

        TapToStartAnimate.Instance.DoAnimation();
        yield return new WaitForSeconds(0f);

        
        state = BattleState.PLAYERTURN;
        PlayerTurn();

    }

    void PlayerTurn()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        GlobalTouchController.Instance.EnableTouchController();
        StartCoroutine(PlayerAttack());

    }

    IEnumerator PlayerAttack()
    {
        bool t_IsPlayerSetValue = false;
        powerBar.ShowPowerBar(delegate {
            t_IsPlayerSetValue = true;
            playerUnit.anim.SetTrigger("bashIt");
        });
        powerBar.OnPassingTheAccuracyEvaluation += enemyUnit.TakeDamage;

        WaitUntil t_WaitUntilPlayerSetValue = new WaitUntil(() => {
            if (!t_IsPlayerSetValue)
                return false;
            return true;
        });
        yield return t_WaitUntilPlayerSetValue;
        Debug.Log(t_IsPlayerSetValue);
        startText.enabled = false;
        Debug.Log("Player gave damage");
        yield return new WaitForSeconds(1.8f);
        enemyHUD.SetHealth(enemyUnit.currentHP);
        enemyUnit.anim.SetTrigger("IsDamaged");

        bool isDead = enemyUnit.died;
        if(isDead)
        {
            enemyUnit.anim.SetTrigger("IsDead");
            yield return new WaitForSeconds(2f);
            state = BattleState.WON;
            EndBattle();
        }
        else
        {

            
            yield return new WaitForSeconds(2f);
            CameraTransition.Instance.anim.SetTrigger("EnemyTurn");
            state = BattleState.ENEMYTURN;
            
            
            
            EnemyTurn();
        }
        
        yield return new WaitForSeconds(0f);
    }

    void EnemyTurn()
    {
        //CameraTransition.Instance.EnemyTurn();
        enemyAttacked = true;
        playerAttacked = false;
        GlobalTouchController.Instance.DisableTouchController();
        
        StartCoroutine(EnemyAttack());
    }

    IEnumerator EnemyAttack()
    {
       
        playerUnit.TakeDamage(Random.Range(0.9f, 1f));
        Debug.Log("Enemy Gave Damage");
        enemyUnit.anim.SetTrigger("BashIt");
        yield return new WaitForSeconds(1.8f);
        playerUnit.anim.SetTrigger("HeadHit");
        playerHUD.SetHealth(playerUnit.currentHP);
        
        yield return new WaitForSeconds(1f);

        bool isDead = playerUnit.died;
        if (isDead)
        {
            state = BattleState.LOSE;
            EndBattle();
        }
        else
        {
            yield return new WaitForSeconds(1f);
            CameraTransition.Instance.anim.SetTrigger("PlayerTurn");
            state = BattleState.PLAYERTURN;
            
            
            //powerBar.ShowPowerBar();
            //theButton.SetActive(true);
            PlayerTurn();
        }

    }
    void EndBattle()
    {
        if(state == BattleState.WON)
        {
            
            YouWon.SetActive(true);
            GlobalTouchController.Instance.DisableTouchController();
        }
        else if(state == BattleState.LOSE)
        {

        }
    }
    public void OnTransPree()
    {
        
        
        
        startText.enabled = false;
        
        playerAttacked = true;
        enemyAttacked = false;
        Debug.Log(playerAttacked);
    }

    public void OnNextLevelButtonPress()
    {
        DestroyImmediate(GameObject.FindGameObjectWithTag("Enemy"));
        DestroyImmediate(GameObject.FindGameObjectWithTag("Player"));
        YouWon.SetActive(false);
        faceIconOne.SetActive(false);
        faceiconTwo.SetActive(true);

        lvlText.text = "Level 02";
        state = BattleState.START;
        StartCoroutine(SetupBattle());

    }

    


}
