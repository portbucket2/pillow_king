﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapToStartAnimate : MonoBehaviour
{
    public static TapToStartAnimate Instance;
    public Animator anim;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    public void DoAnimation()
    {
        anim.SetBool("IsTapped", true);
    }
}
